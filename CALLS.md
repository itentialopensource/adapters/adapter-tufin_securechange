## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Tufin SecureChange. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Tufin SecureChange.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the TufinSecureChange. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAnExistingApplicationAccessRequestByID(applicationId, requestId, callback)</td>
    <td style="padding:15px">Retrieve an existing application access request by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_access_requests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnApplicationAccessRequest(body, applicationId, requestId, callback)</td>
    <td style="padding:15px">Update an application access request</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_access_requests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingApplicationAccessRequests(applicationId, callback)</td>
    <td style="padding:15px">Retrieve existing application access requests</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_access_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplicationAccessRequests(body, applicationId, callback)</td>
    <td style="padding:15px">Update application access requests</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_access_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplicationAccessRequest(body, applicationId, callback)</td>
    <td style="padding:15px">Create application access request</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_access_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingConnectionsWithExtendedResourcesInformation(applicationId, callback)</td>
    <td style="padding:15px">Retrieve existing connections, with extended resources information</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections_extended?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createATicketToRepairAConnection(body, applicationId, connectionId, callback)</td>
    <td style="padding:15px">Create a ticket to repair a connection</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections/{pathv2}/repair?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingConnections(applicationId, callback)</td>
    <td style="padding:15px">Retrieve existing connections</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConnections(body, applicationId, callback)</td>
    <td style="padding:15px">Create connections</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConnections(body, applicationId, callback)</td>
    <td style="padding:15px">Update connections</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAnExistingConnectionByID(applicationId, connectionId, callback)</td>
    <td style="padding:15px">Retrieve an existing connection by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAConnection(body, applicationId, connectionId, callback)</td>
    <td style="padding:15px">Update a connection</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConnection(applicationId, connectionId, callback)</td>
    <td style="padding:15px">Delete connection</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesApplicationIdentities(callback)</td>
    <td style="padding:15px">Fetches application identities</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/application_identities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingInterfaceConnections(applicationId, applicationInterfaceId, callback)</td>
    <td style="padding:15px">Retrieve existing interface connections</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_interfaces/{pathv2}/interface_connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInterfaceConnections(body, applicationId, applicationInterfaceId, callback)</td>
    <td style="padding:15px">Create interface connections</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_interfaces/{pathv2}/interface_connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAnExistingInterfaceConnectionByID(applicationId, applicationInterfaceId, connectionInterfaceId, callback)</td>
    <td style="padding:15px">Retrieve an existing interface connection by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_interfaces/{pathv2}/interface_connections/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInterfaceConnection(applicationId, applicationInterfaceId, connectionInterfaceId, callback)</td>
    <td style="padding:15px">Delete interface connection</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_interfaces/{pathv2}/interface_connections/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnInterfaceConnection(body, applicationId, applicationInterfaceId, connectionInterfaceId, callback)</td>
    <td style="padding:15px">Update an interface connection</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_interfaces/{pathv2}/interface_connections/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAConnectionToApplication(body, applicationId, connectionToApplicationId, callback)</td>
    <td style="padding:15px">Update a connection to application</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections_to_applications/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAnExistingConnectionToApplicationByID(applicationId, connectionToApplicationId, callback)</td>
    <td style="padding:15px">Retrieve an existing connection to application by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections_to_applications/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConnectionToApplication(applicationId, connectionToApplicationId, callback)</td>
    <td style="padding:15px">Delete connection to application</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections_to_applications/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplicationInterface(body, applicationId, callback)</td>
    <td style="padding:15px">Create application interface</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingApplicationInterfaces(applicationId, callback)</td>
    <td style="padding:15px">Retrieve existing application interfaces</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplicationInterface(applicationId, applicationInterfaceId, callback)</td>
    <td style="padding:15px">Delete application interface</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnApplicationInterface(body, applicationId, applicationInterfaceId, callback)</td>
    <td style="padding:15px">Update an application interface</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAnExistingApplicationInterfaceByID(applicationId, applicationInterfaceId, callback)</td>
    <td style="padding:15px">Retrieve an existing application interface by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingConnectionsToApplication(applicationId, callback)</td>
    <td style="padding:15px">Retrieve existing connections to application</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections_to_applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConnectionToApplication(body, applicationId, callback)</td>
    <td style="padding:15px">Create connection to application</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections_to_applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConnectionToApplicationMappingsForSourceAndTargetApplications(body, sourceApplicationId, targetApplicationId, callback)</td>
    <td style="padding:15px">Update connection to application mappings for source and target applications</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/migrate/{pathv1},/{pathv2}/connections_to_applications_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveConnectionToApplicationMappingsForSourceAndTargetApplications(sourceApplicationId, targetApplicationId, callback)</td>
    <td style="padding:15px">Retrieve connection to application mappings for source and target applications</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/migrate/{pathv1},/{pathv2}/connections_to_applications_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copyApplicationServersAndConnectionsToAnotherApplication(body, sourceApplicationId, targetApplicationId, callback)</td>
    <td style="padding:15px">Copy application servers and connections to another application.</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/migrate/{pathv1},/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServerMappingsForSourceAndTargetApplications(body, sourceApplicationId, targetApplicationId, callback)</td>
    <td style="padding:15px">Update server mappings for source and target applications</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/migrate/{pathv1},/{pathv2}/servers_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveServerMappingsForSourceAndTargetApplications(sourceApplicationId, targetApplicationId, callback)</td>
    <td style="padding:15px">Retrieve server mappings for source and target applications</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/migrate/{pathv1},/{pathv2}/servers_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnApplicationPack(body, customerId, applicationPackId, callback)</td>
    <td style="padding:15px">Update an application pack</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplicationPack(customerId, applicationPackId, callback)</td>
    <td style="padding:15px">Delete application pack</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAnExistingApplicationPackByID(customerId, applicationPackId, callback)</td>
    <td style="padding:15px">Retrieve an existing application pack by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplicationInterfacesAndTagMapping(body, customerId, applicationPackId, callback)</td>
    <td style="padding:15px">Update application interfaces and tag mapping</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/{pathv2}/application_interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeApplicationInterfacesFromApplicationPack(customerId, applicationPackId, callback)</td>
    <td style="padding:15px">Remove application interfaces from application pack</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/{pathv2}/application_interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllApplicationPackTags(customerId, applicationPackId, callback)</td>
    <td style="padding:15px">List all  application pack tags</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyTagsOfAnApplicationPack(body, customerId, applicationPackId, callback)</td>
    <td style="padding:15px">Modify tags of an application pack</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewTagUnderAnApplicationPack(body, customerId, applicationPackId, callback)</td>
    <td style="padding:15px">Create new Tag under an application pack</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTagsFromApplicationPack(customerId, applicationPackId, callback)</td>
    <td style="padding:15px">Delete tags from application pack</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAConnectionToApplicationPack(applicationId, connectionToApplicationPackId, callback)</td>
    <td style="padding:15px">Delete a connection to application pack</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connection_to_application_packs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAnExistingConnectionToApplicationPackByID(applicationId, connectionToApplicationPackId, callback)</td>
    <td style="padding:15px">Retrieve an existing connection to application pack by ID.</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connection_to_application_packs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSecurechangeworkflowApiSecureappRepositoryApplicationsApplicationIdConnectionToApplicationPacksConnectionToApplicationPackId(body, applicationId, connectionToApplicationPackId, callback)</td>
    <td style="padding:15px">Update an application pack</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connection_to_application_packs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplicationPacks(body, customerId, callback)</td>
    <td style="padding:15px">Update application packs</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllApplicationPacks(customerId, callback)</td>
    <td style="padding:15px">List all application packs</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewApplicationPacks(body, customerId, callback)</td>
    <td style="padding:15px">Create new application packs</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagByID(customerId, applicationPackId, tagId, callback)</td>
    <td style="padding:15px">Get tag by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/{pathv2}/tags/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewConnectionToApplicationPacks(body, applicationId, callback)</td>
    <td style="padding:15px">Create new connection to application packs</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connection_to_application_packs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAListOfAllConnectionToApplicationPackByApplicationID(applicationId, callback)</td>
    <td style="padding:15px">Retrieve a list of all connection to application pack by application ID.</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connection_to_application_packs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingAccessRules(applicationId, callback)</td>
    <td style="padding:15px">Retrieve existing access rules</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/access_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesServers(callback)</td>
    <td style="padding:15px">Fetches servers</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/network_objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesServerByID(serverId, callback)</td>
    <td style="padding:15px">Fetches server by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/network_objects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingServers(applicationId, callback)</td>
    <td style="padding:15px">Retrieve existing servers</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/network_objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServers(body, applicationId, callback)</td>
    <td style="padding:15px">Update servers</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/network_objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServers(body, applicationId, callback)</td>
    <td style="padding:15px">Create servers</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/network_objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServer(applicationId, serverId, callback)</td>
    <td style="padding:15px">Delete server</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/network_objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAnExistingServerByID(applicationId, serverId, callback)</td>
    <td style="padding:15px">Retrieve an existing server by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/network_objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ifTheIdentifiedServerIsAGroupReturnAListOfItsMembersWhenTheMediaTypeIsTextPlainDefaultPrintEachMemberOnASeparateLine(applicationId, serverId, callback)</td>
    <td style="padding:15px">If the identified server is a group, return a list of its members. When the media type is 'text/plain' (default), print each member on a separate line.</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/network_objects/{pathv2}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveServer(body, applicationId, callback)</td>
    <td style="padding:15px">Move server</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/move_server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportServerImpactAnalysisToACSVFile(applicationId, serverId, callback)</td>
    <td style="padding:15px">Export Server Impact Analysis to a CSV file.</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/network_objects/{pathv2}/impact_analysis_csv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyUnusedAndNonPredefinedGlobalServices(body, callback)</td>
    <td style="padding:15px">Modify unused and non predefined global services</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUnusedAndNonPredefinedGlobalServiceByName(callback)</td>
    <td style="padding:15px">Delete unused and non predefined global service by name</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesServices(callback)</td>
    <td style="padding:15px">Fetches services</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGlobalServices(body, callback)</td>
    <td style="padding:15px">Create global services</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesServiceByID(serviceId, callback)</td>
    <td style="padding:15px">Fetches service by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUnusedAndNonPredefinedGlobalService(serviceId, callback)</td>
    <td style="padding:15px">Delete unused and non predefined global service</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingServices(applicationId, callback)</td>
    <td style="padding:15px">Retrieve existing services</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServices(body, applicationId, callback)</td>
    <td style="padding:15px">Create services</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServices(body, applicationId, callback)</td>
    <td style="padding:15px">Update services</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteService(applicationId, serviceId, callback)</td>
    <td style="padding:15px">Delete service</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAnExistingServiceByID(applicationId, serviceId, callback)</td>
    <td style="padding:15px">Retrieve an existing service by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesUsers(callback)</td>
    <td style="padding:15px">Fetches users</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUsers(body, callback)</td>
    <td style="padding:15px">Create users</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesUserByID(userId, callback)</td>
    <td style="padding:15px">Fetches user by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUnusedUser(userId, callback)</td>
    <td style="padding:15px">Delete unused user</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveSecurityPolicyViolationsFromSecureTrack(applicationId, callback)</td>
    <td style="padding:15px">Retrieve security policy violations from SecureTrack</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/compliance_violations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllApplications(callback)</td>
    <td style="padding:15px">List all applications</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewApplications(body, callback)</td>
    <td style="padding:15px">Create new applications</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplications(body, callback)</td>
    <td style="padding:15px">Update applications</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnApplication(applicationId, callback)</td>
    <td style="padding:15px">Delete an application</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAnExistingApplicationByID(applicationId, callback)</td>
    <td style="padding:15px">Retrieve an existing application by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeAnApplication(body, applicationId, callback)</td>
    <td style="padding:15px">Change an application</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportApplicationHistoryToPDF(applicationId, callback)</td>
    <td style="padding:15px">Export application history to PDF</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/history/export_pdf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveApplicationsToAnotherCustomer(body, callback)</td>
    <td style="padding:15px">Move applications to another customer</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/move_applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveApplicationHistory(applicationId, callback)</td>
    <td style="padding:15px">Retrieve application history</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportImpactAnalysisToACSVFile(applicationId, callback)</td>
    <td style="padding:15px">Export Impact Analysis to a CSV file.</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/impact_analysis_csv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">autoAssociateCloudServersFromSecureTrackWithAnApplication(body, callback)</td>
    <td style="padding:15px">Auto associate cloud servers from SecureTrack with an application</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/cloud_console/servers/auto_association_rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveCloudServerListingFromSecureTrack(callback)</td>
    <td style="padding:15px">Retrieve cloud server listing from SecureTrack</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/cloud_console/servers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateCloudServersFromSecureTrackWithAnApplication(body, callback)</td>
    <td style="padding:15px">Associate cloud servers from SecureTrack with an application</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/cloud_console/servers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importCustomers(body, callback)</td>
    <td style="padding:15px">import customers</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/customers/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllCustomers(callback)</td>
    <td style="padding:15px">List all customers</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/customers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingApplicationsForSpecificDomain(customerId, callback)</td>
    <td style="padding:15px">Retrieve existing applications for specific domain</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/customers/{pathv1}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllImportedCustomers(callback)</td>
    <td style="padding:15px">List all imported customers</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/customers/imported?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesCustomerByID(customerId, callback)</td>
    <td style="padding:15px">Fetches customer by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/customers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateACustomer(body, customerId, callback)</td>
    <td style="padding:15px">Update a customer</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/customers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllNonImportedCustomers(callback)</td>
    <td style="padding:15px">List all non-imported customers</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/customers/not_imported?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">synchronizeDomains(body, callback)</td>
    <td style="padding:15px">synchronize domains</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/domains/synchronize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllDomains(callback)</td>
    <td style="padding:15px">List all domains</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/domains/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesDomainByID(domainId, callback)</td>
    <td style="padding:15px">Fetches domain by ID</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesVirtualServersFromSecureTrack(callback)</td>
    <td style="padding:15px">Fetches virtual servers from SecureTrack</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/external_resources/f5_virtual_servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfTheDevicesThatYouCanSelectAsATargetInSecureChange(callback)</td>
    <td style="padding:15px">Returns the details of the devices that you can select as a target in SecureChange</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSuggestedTargetsForAGivenAccessRequest(body, callback)</td>
    <td style="padding:15px">Get suggested targets for a given access request</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/devices/suggest_target?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheWholeListOfExcludedDevicesInSecureChange(body, callback)</td>
    <td style="padding:15px">Update the whole list of excluded devices in SecureChange</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/devices/excluded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsIDsForSpecifiedListOfExcludedParentManagementDevicesAndChildFirewallsInSecureChange(callback)</td>
    <td style="padding:15px">Returns IDs for specified list of excluded parent management devices and child firewalls in SecureChange</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/devices/excluded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runServerDecommissionDesignerForGivenTask(body, id, taskId, callback)</td>
    <td style="padding:15px">Run server decommission designer for given task</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/current/tasks/{pathv2}/multi_server_decommission_request/designer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runImpactAnalysisForGivenTask(body, id, taskId, callback)</td>
    <td style="padding:15px">Run Impact Analysis for given task</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/current/tasks/{pathv2}/multi_server_decommission_request/impact_analysis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImpactAnalysisResults(id, stepId, taskId, fieldId, callback)</td>
    <td style="padding:15px">Get impact analysis results</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/multi_server_decommission_request/{pathv4}/impact_analysis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runVerifierForGivenTask(body, id, taskId, callback)</td>
    <td style="padding:15px">Run Verifier for given task</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/current/tasks/{pathv2}/multi_server_decommission_request/verifier?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVerifierResults(id, stepId, taskId, fieldId, callback)</td>
    <td style="padding:15px">Get verifier results</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/multi_server_decommission_request/{pathv4}/verifier?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerDecommissionDesignerResults(id, stepId, taskId, fieldId, callback)</td>
    <td style="padding:15px">Get server decommission designer results</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/multi_server_decommission_request/{pathv4}/designer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyTheSecureTrackConnectionSettingsAndTestTheConnectionStatus(body, callback)</td>
    <td style="padding:15px">Modify the SecureTrack connection settings and test the connection status</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/integration_with_secureTrack/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveTheSecureTrackConnectionSettingsAndStatus(callback)</td>
    <td style="padding:15px">Retrieve the SecureTrack connection settings and status</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/integration_with_secureTrack/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importUserFromLDAP(body, callback)</td>
    <td style="padding:15px">Import user from LDAP</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheListOfUsers(callback)</td>
    <td style="padding:15px">Return the list of users</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheSpecifiedUser(id, callback)</td>
    <td style="padding:15px">Return the specified user</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rejectATicket(body, id, callback)</td>
    <td style="padding:15px">Reject a ticket</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/reject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mapRulesToTicket(body, id, callback)</td>
    <td style="padding:15px">Map rules to ticket</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/map_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reassignsTheSpecifiedTicketTaskToTheSpecifiedUser(body, id, stepId, taskId, assigneeId, callback)</td>
    <td style="padding:15px">Reassigns the specified ticket task to the specified user.</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/reassign/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confirmTheRequest(body, id, callback)</td>
    <td style="padding:15px">Confirm the request</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/confirm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeTheRequesterOfTheSpecifiedTicketTaskToTheSpecifiedUser(body, id, assigneeId, callback)</td>
    <td style="padding:15px">Change the requester of the specified ticket task to the specified user.</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/change_requester/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheSpecifiedTicketToTheSpecifiedStep(body, id, stepId, taskId, toStepId, callback)</td>
    <td style="padding:15px">Returns the specified ticket to the specified step</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/redo/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheHistoryOfTheSpecifiedTicket(id, callback)</td>
    <td style="padding:15px">Returns the history of the specified ticket</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelATicket(body, id, callback)</td>
    <td style="padding:15px">Cancel a ticket</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfATheFields(id, stepId, taskId, callback)</td>
    <td style="padding:15px">Returns the details of a the fields</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeMultipleFieldsWithinATaskOfATicket(body, id, stepId, taskId, callback)</td>
    <td style="padding:15px">Change multiple fields within a task of a ticket</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommandsForDeviceFromDesignerResultsOfASpecificFieldId(id, stepId, taskId, fieldId, deviceId, callback)</td>
    <td style="padding:15px">Get commands for device from designer results of a specific field id</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/fields/{pathv4}/designer/device/{pathv5}/commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeTheTicketProperties(body, id, callback)</td>
    <td style="padding:15px">Change the ticket properties</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/ticket_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeATaskOfATicketAtTheCurrentStep(body, id, taskId, callback)</td>
    <td style="padding:15px">Change a task of a ticket at the current step</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/current/tasks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedField(id, stepId, taskId, fieldId, callback)</td>
    <td style="padding:15px">Returns the details of a specified field</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/fields/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeAFieldWithinATaskOfATicket(body, id, stepId, taskId, fieldId, callback)</td>
    <td style="padding:15px">Change a field within a task of a ticket</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/fields/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRuleDecommisionVerifierResults(id, stepId, taskId, callback)</td>
    <td style="padding:15px">Get rule decommision verifier results</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/rule_decommission/verifier?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDesignerResultsAccordingToFieldId(id, stepId, taskId, fieldId, callback)</td>
    <td style="padding:15px">Get designer results according to field id</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/fields/{pathv4}/designer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheResultsOfTheCommitAction(id, stepId, taskId, fieldId, managementId, callback)</td>
    <td style="padding:15px">Returns the results of the Commit action</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/fields/{pathv4}/designer/commit_report/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedTicketStages(id, callback)</td>
    <td style="padding:15px">Returns the details of a specified ticket stages</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeAFieldWithinATaskOfATicketAtTheCurrentStep(body, id, taskId, fieldId, callback)</td>
    <td style="padding:15px">Change a field within a task of a ticket at the current step</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/current/tasks/{pathv2}/fields/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeMultipleFieldWithinATaskOfATicketInTheCurrentStep(body, id, taskId, callback)</td>
    <td style="padding:15px">Change multiple field within a task of a ticket in the current step</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/current/tasks/{pathv2}/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicy(body, id, taskId, instructionId, callback)</td>
    <td style="padding:15px">Modify the Designer suggestions for Access Request or Clone Server Policy</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/tasks/{pathv2}/designer/instruction/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepId(id, stepId, callback)</td>
    <td style="padding:15px">Returns the details of a specified ticket stages</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsAListOfTicketsBySearchParameters(callback)</td>
    <td style="padding:15px">Returns a list of tickets by search parameters</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/free_text_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerClonePolicyVerifierResults(id, stepId, taskId, callback)</td>
    <td style="padding:15px">Get server clone policy verifier results</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/clone_server_policy/verifier?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedTicket(id, callback)</td>
    <td style="padding:15px">Returns the details of a specified ticket</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">submitANewTicketToSecureChange(body, callback)</td>
    <td style="padding:15px">Submit a new ticket to Secure Change</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedTicketsMax100TicketsInASingleQuery(callback)</td>
    <td style="padding:15px">Returns the details of a specified tickets (max 100 tickets in a single query)</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurechangeworkflowApiSecurechangeTicketsSearch(callback)</td>
    <td style="padding:15px">Returns a list of tickets by search parameters</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTopologyMapImagePngFileForSpecificVerifierResults(id, stepId, taskId, fieldId, callback)</td>
    <td style="padding:15px">Returns topology map image (png file) for specific verifier results</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/multi_access_request/{pathv4}/verifier/topology_map?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessRequestVerifierResults(id, stepId, taskId, fieldId, callback)</td>
    <td style="padding:15px">Get access request verifier results</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/multi_access_request/{pathv4}/verifier?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedTask(id, stepId, taskId, callback)</td>
    <td style="padding:15px">Returns the details of a specified task</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeATaskOfATicket(body, id, stepId, taskId, callback)</td>
    <td style="padding:15px">Change a task of a ticket</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfTheTasks(id, stepId, callback)</td>
    <td style="padding:15px">Returns the details of the tasks</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServersMapping(sourceApplicationId, targetApplicationId, queryObject, callback)</td>
    <td style="padding:15px">getServersMapping</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/migrate/{pathv1},{pathv2}/servers_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyServersMapping(sourceApplicationId, targetApplicationId, body, callback)</td>
    <td style="padding:15px">modifyServersMapping</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/migrate/{pathv1},{pathv2}/servers_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceInstanceMapping(sourceApplicationId, targetApplicationId, callback)</td>
    <td style="padding:15px">getInterfaceInstanceMapping</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/migrate/{pathv1},{pathv2}/connections_to_applications_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyInterfaceInstanceMapping(sourceApplicationId, targetApplicationId, body, callback)</td>
    <td style="padding:15px">modifyInterfaceInstanceMapping</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/migrate/{pathv1},{pathv2}/connections_to_applications_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrateApplication(sourceApplicationId, targetApplicationId, callback)</td>
    <td style="padding:15px">migrateApplication</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/migrate/{pathv1},{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">openUploadForm(callback)</td>
    <td style="padding:15px">openUploadForm</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/import_applications/upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadExcel(body, callback)</td>
    <td style="padding:15px">uploadExcel</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/import_applications/upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadEmptyExcel(callback)</td>
    <td style="padding:15px">downloadEmptyExcel</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/import_applications/download_template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllApplicationsQuery(queryObject, callback)</td>
    <td style="padding:15px">listAllApplicationsQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveApplicationHistoryQuery(applicationId, queryObject, callback)</td>
    <td style="padding:15px">retrieveApplicationHistoryQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportApplicationHistoryToPDFQuery(applicationId, queryObject, callback)</td>
    <td style="padding:15px">exportApplicationHistoryToPDFQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/history/export_pdf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileAttachment(uid, callback)</td>
    <td style="padding:15px">getFileAttachment</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/attachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAttachment(body, callback)</td>
    <td style="padding:15px">addAttachment</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/attachments/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runRuleRecertificationUpdateMetadata(id, taskId, callback)</td>
    <td style="padding:15px">runRuleRecertificationUpdateMetadata</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/current/tasks/{pathv2}/rule_recertification/update_metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRelatedRules(id, stepId, taskId, fieldId, callback)</td>
    <td style="padding:15px">getRelatedRules</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/fields/{pathv4}/related_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityZones(id, stepId, taskId, fieldId, callback)</td>
    <td style="padding:15px">getSecurityZones</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/fields/{pathv4}/security_zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDesignerResultsByDeviceId(id, stepId, taskId, fieldId, managementId, callback)</td>
    <td style="padding:15px">getDesignerResultsByDeviceId</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/fields/{pathv4}/designer/device/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addComment(ticketId, stepId, taskId, body, callback)</td>
    <td style="padding:15px">addComment</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteComment(ticketId, commentId, callback)</td>
    <td style="padding:15px">deleteComment</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/comments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedTicketQuery(id, queryObject, callback)</td>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedTicketQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurechangeworkflowApiSecurechangeTicketsSearchQuery(queryObject, callback)</td>
    <td style="padding:15px">getSecurechangeworkflowApiSecurechangeTicketsSearchQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedFieldQuery(id, stepId, taskId, fieldId, queryObject, callback)</td>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedFieldQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/fields/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedTicketStagesQuery(id, queryObject, callback)</td>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedTicketStagesQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicyQuery(id, taskId, instructionId, queryObject, body, callback)</td>
    <td style="padding:15px">modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicyQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/tasks/{pathv2}/designer/instruction/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedTicketsMax100TicketsInASingleQueryQuery(queryObject, callback)</td>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedTicketsMax100TicketsInASingleQueryQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">submitANewTicketToSecureChangeQuery(queryObject, body, callback)</td>
    <td style="padding:15px">submitANewTicketToSecureChangeQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepIdQuery(id, stepId, queryObject, callback)</td>
    <td style="padding:15px">getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepIdQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfTheTasksQuery(id, stepId, queryObject, callback)</td>
    <td style="padding:15px">returnsTheDetailsOfTheTasksQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedTaskQuery(id, stepId, taskId, queryObject, callback)</td>
    <td style="padding:15px">returnsTheDetailsOfASpecifiedTaskQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfATheFieldsQuery(id, stepId, taskId, queryObject, callback)</td>
    <td style="padding:15px">returnsTheDetailsOfATheFieldsQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/steps/{pathv2}/tasks/{pathv3}/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsAListOfTicketsBySearchParametersQuery(queryObject, callback)</td>
    <td style="padding:15px">returnsAListOfTicketsBySearchParametersQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/free_text_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingConnectionsQuery(applicationId, queryObject, callback)</td>
    <td style="padding:15px">retrieveExistingConnectionsQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingConnectionsWithExtendedResourcesInformationQuery(applicationId, queryObject, callback)</td>
    <td style="padding:15px">retrieveExistingConnectionsWithExtendedResourcesInformationQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections_extended?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesApplicationIdentitiesQuery(queryObject, callback)</td>
    <td style="padding:15px">fetchesApplicationIdentitiesQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/application_identities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingInterfaceConnectionsQuery(applicationId, applicationInterfaceId, queryObject, callback)</td>
    <td style="padding:15px">retrieveExistingInterfaceConnectionsQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_interfaces/{pathv2}/interface_connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingApplicationInterfacesQuery(applicationId, queryObject, callback)</td>
    <td style="padding:15px">retrieveExistingApplicationInterfacesQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/application_interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingConnectionsToApplicationQuery(applicationId, queryObject, callback)</td>
    <td style="padding:15px">retrieveExistingConnectionsToApplicationQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connections_to_applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAListOfAllConnectionToApplicationPackByApplicationIDQuery(applicationId, queryObject, callback)</td>
    <td style="padding:15px">retrieveAListOfAllConnectionToApplicationPackByApplicationIDQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/connection_to_application_packs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTagsFromApplicationPackQuery(customerId, applicationPackId, queryObject, callback)</td>
    <td style="padding:15px">deleteTagsFromApplicationPackQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllApplicationPacksQuery(customerId, queryObject, callback)</td>
    <td style="padding:15px">listAllApplicationPacksQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeApplicationInterfacesFromApplicationPackQuery(customerId, applicationPackId, queryObject, callback)</td>
    <td style="padding:15px">removeApplicationInterfacesFromApplicationPackQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/customers/{pathv1}/application_packs/{pathv2}/application_interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesServersQuery(queryObject, callback)</td>
    <td style="padding:15px">fetchesServersQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/network_objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingServersQuery(applicationId, queryObject, callback)</td>
    <td style="padding:15px">retrieveExistingServersQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/network_objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServerQuery(applicationId, serverId, queryObject, callback)</td>
    <td style="padding:15px">deleteServerQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/network_objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesServicesQuery(queryObject, callback)</td>
    <td style="padding:15px">fetchesServicesQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUnusedAndNonPredefinedGlobalServiceByNameQuery(queryObject, callback)</td>
    <td style="padding:15px">deleteUnusedAndNonPredefinedGlobalServiceByNameQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceQuery(applicationId, serviceId, queryObject, callback)</td>
    <td style="padding:15px">deleteServiceQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveExistingServicesQuery(applicationId, queryObject, callback)</td>
    <td style="padding:15px">retrieveExistingServicesQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesUsersQuery(queryObject, callback)</td>
    <td style="padding:15px">fetchesUsersQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllChangesAudits(queryObject, callback)</td>
    <td style="padding:15px">getAllChangesAudits</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/change_audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveCloudServerListingFromSecureTrackQuery(queryObject, callback)</td>
    <td style="padding:15px">retrieveCloudServerListingFromSecureTrackQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/cloud_console/servers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateCloudServersFromSecureTrackWithAnApplicationQuery(queryObject, body, callback)</td>
    <td style="padding:15px">associateCloudServersFromSecureTrackWithAnApplicationQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/cloud_console/servers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllCustomersQuery(queryObject, callback)</td>
    <td style="padding:15px">listAllCustomersQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/customers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllDomainsQuery(queryObject, callback)</td>
    <td style="padding:15px">listAllDomainsQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/domains/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResources(queryObject, callback)</td>
    <td style="padding:15px">getResources</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/externalprovider/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRole(id, queryObject, callback)</td>
    <td style="padding:15px">getRole</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoles(queryObject, callback)</td>
    <td style="padding:15px">getRoles</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheDetailsOfTheDevicesThatYouCanSelectAsATargetInSecureChangeQuery(queryObject, callback)</td>
    <td style="padding:15px">returnsTheDetailsOfTheDevicesThatYouCanSelectAsATargetInSecureChangeQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyTheSecureTrackConnectionSettingsAndTestTheConnectionStatusQuery(queryObject, body, callback)</td>
    <td style="padding:15px">modifyTheSecureTrackConnectionSettingsAndTestTheConnectionStatusQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/integration_with_secureTrack/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheListOfUsersQuery(queryObject, callback)</td>
    <td style="padding:15px">returnTheListOfUsersQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getParty(id, showIndirectRelation, showPermissions, callback)</td>
    <td style="padding:15px">getParty</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteParty(id, callback)</td>
    <td style="padding:15px">deleteParty</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveWorkflowsByType(queryObject, callback)</td>
    <td style="padding:15px">getActiveWorkflowsByType</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/workflows/active_workflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesVirtualServersFromSecureTrackQuery(queryObject, callback)</td>
    <td style="padding:15px">fetchesVirtualServersFromSecureTrackQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/external_resources/f5_virtual_servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsIDsForSpecifiedListOfExcludedParentManagementDevicesAndChildFirewallsInSecureChangeQuery(queryObject, callback)</td>
    <td style="padding:15px">returnsIDsForSpecifiedListOfExcludedParentManagementDevicesAndChildFirewallsInSecureChangeQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/devices/excluded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSuggestedTargetsForAGivenAccessRequestQuery(queryObject, body, callback)</td>
    <td style="padding:15px">getSuggestedTargetsForAGivenAccessRequestQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/devices/suggest_target?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importLdap(body, callback)</td>
    <td style="padding:15px">importLdap</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/users/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeGroupMembers(id, body, callback)</td>
    <td style="padding:15px">changeGroupMembers</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/users/group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLocalGroup(body, callback)</td>
    <td style="padding:15px">createLocalGroup</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/users/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePartyRoles(id, body, callback)</td>
    <td style="padding:15px">updatePartyRoles</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelTicket(id, queryObject, callback)</td>
    <td style="padding:15px">cancelTicket</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rejectATicketQuery(id, queryObject, body, callback)</td>
    <td style="padding:15px">rejectATicketQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/reject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confirmTheRequestQuery(id, queryObject, body, callback)</td>
    <td style="padding:15px">confirmTheRequestQuery</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/confirm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mapRules(id, queryObject, callback)</td>
    <td style="padding:15px">mapRules</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/tickets/{pathv1}/map_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTriggers(queryObject, callback)</td>
    <td style="padding:15px">getTriggers</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/triggers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTriggers(body, callback)</td>
    <td style="padding:15px">addTriggers</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/securechange/triggers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveServerQuery(applicationId, queryObject, callback)</td>
    <td style="padding:15px">moveServer</td>
    <td style="padding:15px">{base_path}/{version}/securechangeworkflow/api/secureapp/repository/applications/{pathv1}/move_server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
