# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the TufinSecureChange System. The API that was used to build the adapter for TufinSecureChange is usually available in the report directory of this adapter. The adapter utilizes the TufinSecureChange API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Tufin SecureChange adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Tufin SecureChange to provide comprehensive security policy management capabilities.

With this adapter you have the ability to perform operations with Tufin SecureChange such as:

- Retrieve, update, or create applications
- Customers
- Domains
- Retrieve, update, or submit a ticket

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
